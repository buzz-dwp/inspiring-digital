﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InspiringDigital.Startup))]
namespace InspiringDigital
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
