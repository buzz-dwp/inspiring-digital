﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiringDigital.Controllers
{
    public class ActivityController : Controller
    {
        //
        // GET: /Activity/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Browser(int id = 1)
        {
            ViewBag.Title = "Undertanding the browser";
            return View("Browser" + id);
        }

        public ActionResult NewTab()
        {
            return View();
        }
	}
}